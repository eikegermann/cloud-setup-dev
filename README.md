# cloud-setup-dev

This repo is cloned in the ec2 user data to set up dev machines. The entrypoint it `setup.sh`

## Steps

- Add user into IAM role in AWS
- Add users public key into S3
- Rerun the create_users.py script if new users added
- Build instances using template when needed (just pick instance size)
- Assign an elastic IP to an instance if you wish
- Set up your ~/.ssh/config
