#!/bin/bash
S3_BUCKET_PUB_KEYS="cg-apac-invent-sva-docclass-dev-machine-setup"

sudo apt-get -y update
sudo apt-get install -y git-lfs \
                        docker \
                        docker-compose \
                        python3-pip
sudo git lfs install
pip3 install boto3

sudo groupadd docker

# Install Nvidia drivers for docker if GPU is available
if lspci | grep "3D controller" 
then
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.1.243-1_amd64.deb
    sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
    sudo dpkg -i cuda-repo-ubuntu1804_10.1.243-1_amd64.deb
    sudo apt-get update
    wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
    sudo apt install ./nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
    sudo apt-get update

    sudo apt-get install -y --no-install-recommends nvidia-driver-430

    sudo nvidia-smi -acp UNRESTRICTED

    # Add the package repositories
    distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
    curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
    sudo apt-get -y update && sudo apt-get install -y nvidia-container-toolkit
    sudo systemctl restart docker
fi

# Creating users
sudo python3 create_users.py --bucket $S3_BUCKET_PUB_KEYS


